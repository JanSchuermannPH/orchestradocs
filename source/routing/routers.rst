Routers
=======

What are Routers?
-----------------

Routers are StandAlone-Classes in which you can define Routes and corresponding Route-Functions.

How to create a Router?
-----------------------

You can use the :doc:`CLI <../cli/using_the_cli>` or simply create a new class based on the Router-Skeleton.

A new Router must extend the :literal:`BaseRoute`-Class

.. code-block:: php
   :linenos:

	<?php

	namespace Routes;

	use Orchestra\Core\Router\BaseRoute;

	class YOUR_ROUTER_NAME extends BaseRoute
	{
		protected $routes = [
			'TARGET' => [
				'METHOD' ,
				'NAME' ,
				'ACTION'
			]
		];
	}

How to register a new Route?
----------------------------

Routes can be registered in two different ways:

Register your routes within the :literal:`$routes`-property on the class:

.. code-block:: php
   :linenos:

	<?php

	protected $routes = [
			'TARGET' => [
				'METHOD' ,
				'NAME' ,
				'ACTION'
			]
		];

:literal:`$routes` must be an associative Array containing a nested Array for each Route.

Register routes in the :literal:`boot`-Method of the class

Every Router can implement a :literal:`boot`-Method that gives access to the Route-Modifier-Functions.

Example:

.. code-block:: php
   :linenos:

	<?php

	public function boot(){
		$this->addRoute(['TARGET' => ['METHOD','NAME','ACTION']]);
	}

How to register an action to a new Router?
------------------------------------------

Actions can be defined either inside the Router itself, or in a separate Controller - just as you like [#performance]_.

* :doc:`Router-Based Routes <router_based_routes>`
* :doc:`Controller-Based Routes <controller_based_routes>`

.. [#performance] :subscript:`Which option you choose has almost no effect on the perfomance, though Router-Based Routes are executed a little faster and they spare memory since the Controller doesn't have to be instantiated`
